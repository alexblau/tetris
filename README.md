## CuTretris

Tetris game in python

## Keybindings

w/UP -> change rotation

a/LEFT -> move piece to the left

d/RIGHT -> move piece to the right

s/DOWN -> lower piece

SPACEBAR -> drop piece to the floor

p -> pause game

r -> restart game

esc -> exit game
## TODO

Change background

Improve score and next piece indicators

## Known bugs

None
